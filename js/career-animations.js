// slide & fade content
jQuery(document).ready(function($) {
	$('.more').on('click', function() {
		var href = $(this).attr('href');

		if(href == "#default-view") {
			$('#ajax').hide();
			$('#default-view').show();
		} else {
			if ($('#ajax').is(':visible')) {
				$('#ajax').css({ display:'block' }).animate({ height:'0' }).empty();
			}

			$('#ajax').css({ display:'block' }).animate({ height:'200px' },function() {
				$('#ajax').load('careers/careers.html ' + href, function() {
					$('#default-view').hide();
					$('#ajax').show();
				});
			});
		}

	});
});